FROM jupyter/scipy-notebook:latest
RUN python -m pip install geopandas
RUN python -m pip install descartes
RUN mkdir src
WORKDIR src/
COPY *.ipynb .
COPY data data