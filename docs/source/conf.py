# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'The Working Group Blog'
copyright = '2020, The Working Group'
author = 'The Working Group'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['recommonmark','jupyter_sphinx','nbsphinx']

extensions += ['sphinxjp.themes.basicstrap']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
##
html_theme = 'alabaster'

html_theme_options = {
    #'logo': 'logo.png',
    'github_user': 'bitprophet',
    'github_repo': 'alabaster',
    'fixed_sidebar': 'true',
    'sidebar_width': '350px',
    'page_width' : '1200px',
    'sidebar_list' : '#333',
}

# html_theme = 'basicstrap'
# html_theme_options = {
#   'header_inverse': True,
#   'relbar_inverse': True,
# }


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_css_files = ['https://cdn.jupyter.org/notebook/5.4.0/style/style.min.css']


def setup(app):
    app.add_css_file(
        'https://cdn.jupyter.org/notebook/5.4.0/style/style.min.css')
